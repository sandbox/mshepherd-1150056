<?php

/**
 * @file
 * Administration page callbacks for the my society module.
 */

/**
 * Form builder. Configure my society.
 *
 * @ingroup forms
 * @see system_settings_form().
 */
function mysociety_admin_settings() {
  drupal_add_css(drupal_get_path('module', 'mysociety') . '/mysociety.css');
  $form['mysociety_writetothem_settings'] = array(
    '#title' => t('Write to them'),
    '#type' => 'fieldset',
    '#description' => t('Settings for the write to them block.'),
    '#collapsible' => TRUE,
    '#collpased' => FALSE,
  );
  $mysociety_writetothem_defaults = mysociety_writetothem_defaults();
  $mysociety_writetothem_options = mysociety_writetothem_options();
  $mysociety_writetothem_values = variable_get('mysociety_writetothem_options', $mysociety_writetothem_defaults);
  $form['mysociety_writetothem_settings']['writetothem_preamble'] = array(
    '#title' => t('Explanatory text'),
    '#type' => 'text_format',
    '#format' => NULL,
    '#description' => t("This text will be displayed before the postcode form on the 'Write to them' block"),
    '#resizable' => FALSE,
    '#rows' => 2,
  );
  $form['mysociety_writetothem_settings']['options'] = array(
    '#title' => t('Options'),
    '#type' => 'checkboxes',
    '#description' => t('You should choose at least one option. If in doubt, select them all.'),
    '#options' => $mysociety_writetothem_options,
    '#default_value' => $mysociety_writetothem_values,
  );
  $form['mysociety_fixmystreet_settings'] = array(
    '#title' => t('Fix my street'),
    '#type' => 'fieldset',
    '#description' => t('Settings for the fix my street block.'),
    '#collapsible' => TRUE,
    '#collpased' => FALSE,
  );
  $form['mysociety_fixmystreet_settings']['fixmystreet_preamble'] = array(
    '#title' => t('Explanatory text'),
    '#type' => 'text_format',
    '#format' => NULL,
    '#description' => t("This text will be displayed before the placename form on the 'Fix my street' block"),
    '#resizable' => FALSE,
    '#rows' => 2,
    '#default_value' => 'matthew',
  );
  $form['#submit'][] = 'mysociety_admin_settings_submit';
  return system_settings_form($form);
}

/**
 * Validate handler for the my society module settings page
 */
function mysociety_admin_settings_validate($form, &$form_state) {
  // Check to be sure that at least one option has been selected from the Write to the configuration options.
  $count_selected_options = 0;
  foreach ($form_state['values']['options'] as $option_name => $option_selected) {
    if ($option_selected != '0') {
      $count_selected_options += 1;
      break;
    }
  }
  if ($count_selected_options == 0) {
    form_set_error('mysociety_writetothem_settings][options', t('Please select at least one item from this list.'));
  }
}

/**
 * Submit handler for the my society module settings page
 */
function mysociety_admin_settings_submit($form, $form_state) {
  variable_set('mysociety_writetothem_options', $form_state['values']['options']);
}

/**
 * Returns array of Write to them options
 * Keeps code tidy further up
 */
function mysociety_writetothem_options() {
  return array (
    'DIW' => t('District Council'),
    'LBO' => t('London Borough'),
    'MTW' => t('Metropolitan District'),
    'UTW' => t('Unitary Authority'),
    'UTE' => t('Unitary Authority'),
    'LGE' => t('Local Government District'),
    'COP' => t('Council of the Isles (Scilly)'),
    'CED' => t('County Councils'),
    'LAC' => t('London Assembly Constituency Members'),
    'LAE' => t('London Assembly List Members'),
    'WAC' => t('Welsh Assembly Constituency Members'),
    'WAE' => t('Welsh Assembly List Members'),
    'SPC' => t('Scottish Parliament Constituency Members'),
    'SPE' => t('Scottish Parliament List Members'),
    'NIE' => t('Northern Ireland Assembly'),
    'WMC' => t('House of Commons'),
    'EUR' => t('European Parliament'),
  );
}
